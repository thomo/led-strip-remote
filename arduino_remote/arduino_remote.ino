/*
 * Based on sources from http://pragprog.com/titles/msard/source_code
 *   Arduino_1_0/RemoteControl/InfraredProxy
 */

#include <SPI.h>
#include <Ethernet.h>
#include <IRremote.h>

#include "index_html.h"

#define IS_EQUAL(a,b) !strcasecmp(a,b)

#define SEND_BUFFER_LENGTH 128
char send_buffer[SEND_BUFFER_LENGTH];

#define IR_CMD_OFF 16728510 
int power_pin = 7;

class InfraredProxy {
  IRsend _infrared_sender;

  // Get characters from client and stores them into buffer until a '\n' is found 
  // (or buffer_length is reached or no further chars are available)
  void read_line(EthernetClient& client, char* buffer, const int buffer_length) {
    int buffer_pos = 0;
    while (client.available() && (buffer_pos < buffer_length - 1)) {
      const char c = client.read();
      if (c == '\n')
        break;
      if (c != '\r')
        buffer[buffer_pos++] = c;
    }
    buffer[buffer_pos] = '\0';
  }

  void process_raw_data(long value) {
    if (value == 0) {
      digitalWrite(power_pin, HIGH);
      _infrared_sender.sendNEC(IR_CMD_OFF, 32);
    } else {
      digitalWrite(power_pin, LOW);
    }
  }

  bool process_command_data(const char* protocol, const int bits, const long value) {
    bool result = true;
    if (!strcasecmp(protocol, "NEC"))
      _infrared_sender.sendNEC(value, bits);
    else if (!strcasecmp(protocol, "SONY"))
      _infrared_sender.sendSony(value, bits);
    else if (!strcasecmp(protocol, "RC5"))
      _infrared_sender.sendRC5(value, bits);
    else if (!strcasecmp(protocol, "RC6"))
      _infrared_sender.sendRC6(value, bits);
    else if (!strcasecmp(protocol, "RAW"))
      process_raw_data(value);
    else
      result = false;
    return result;
  }
  
  bool is_index_request(char* path) {
    return strlen(path) == 0 || IS_EQUAL(path,"/") || IS_EQUAL(path,"/index.html");
  }
  
  void handle_index_request(EthernetClient client, char* path) {
      client.println("HTTP/1.1 200 OK\nServer: arduino\nCache-Control: no-store, no-cache, must-revalidate\nPragma: no-cache\nConnection: close\nContent-Type: text/html\n");

      for (int i = 0; i < INDEX_HTML_TABLE_LENGTH; i++)
      {
          strncpy_P(send_buffer, (char*)pgm_read_word(&(index_html_table[i])),SEND_BUFFER_LENGTH - 1); // Necessary casts and dereferencing, just copy. 
          send_buffer[SEND_BUFFER_LENGTH - 1] ='\0';
          
          client.println(send_buffer);
      }
  }
   
  void handle_command_request(EthernetClient client, char* path) {
      char* args[3];
      for (char** ap = args; (*ap = strsep(&path, "/")) != NULL;)
        if (**ap != '\0')
          if (++ap >= &args[3])
            break;
      const int  bits = atoi(args[1]);
      const long value = atol(args[2]);    
      if (process_command_data(args[0], bits, value)) {
        client.println("HTTP/1.1 200 OK\nConnection: close\n");
      } else {
        client.println("HTTP/1.1 400 Bad Request\nConnection: close\n");
      }
  }
  
  void handle_request(EthernetClient client, char* path) {
 
    if (is_index_request(path)) {
      handle_index_request(client, path);
    } else {
      handle_command_request(client, path);
    }
  }
  
  bool is_empty_line(char* line) {
    return !strcmp(line, "");
  }
  
  public:
  
  // handle requests
  void receive_from_server(EthernetServer server) {
    const int MAX_LINE = 256;
    
    char line[MAX_LINE];
    char cmd_line[MAX_LINE];
    cmd_line[0] = '\0';
    
    EthernetClient client = server.available();
    if (client) {
      while (client.connected()) {
        if (client.available()) {
          read_line(client, line, MAX_LINE);
          
          // check for GET request
          if (line[0] == 'G' && line[1] == 'E' && line[2] == 'T') {
            char* sub = line;
            strsep(&sub, " ");                                        // remove request method (GET )
            strncpy(cmd_line, strsep(&sub, " "), MAX_LINE);           // copy path to cmd_line
            cmd_line[MAX_LINE - 1] = '\0';
          }
          // send response after processing all request lines 
          else if (is_empty_line(line)) {
            handle_request(client, cmd_line);
            break;
          } // else ignore any other line
        }
      }
      delay(1);
      client.stop();
    }
  }  
};

const unsigned int PROXY_PORT = 80;
const unsigned int BAUD_RATE = 9600;

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(10, 0, 0, 111);

EthernetServer server(PROXY_PORT);
InfraredProxy ir_proxy;

void setup() {
  pinMode(power_pin, OUTPUT);  
  Ethernet.begin(mac, ip);
  server.begin();
}

void loop() {
  ir_proxy.receive_from_server(server);
}
