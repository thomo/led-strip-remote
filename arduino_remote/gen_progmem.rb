def escape(line) 
  line.strip().gsub(/\"/,'\"')
end

begin
    if ARGV.size == 0
      raise "missing parameter"
    end
  
    input_file_name = ARGV[0]
    
    var_name = input_file_name.downcase.gsub(/\./,"_");

    puts "#ifndef #{var_name}_h"
    puts "#define #{var_name}_h"
    puts ""
    puts "#include <avr/pgmspace.h>"
    puts ""
    
    file = File.new(input_file_name, "r")
    idx = 0
    while (line = file.gets)
        eline = escape(line)
        next if eline.empty? 
        puts "prog_char #{var_name}_#{idx}[] PROGMEM = \"#{eline}\";"
        idx += 1
    end
    file.close
    
    puts "\nPROGMEM const char *#{var_name}_table[] = \n{\n";
    idx.times do |i|
        puts "#{var_name}_#{i},"
    end
    puts "};"
    
    puts "#define " + "#{var_name}_TABLE_LENGTH".upcase() + " #{idx}"
    
    puts "#endif"
rescue => err
    puts "Error while processing: #{err}"
    err
end