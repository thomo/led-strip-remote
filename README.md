LED Strip with Web Interface
============================

I want to control a LED RGB strip by a web interface. My setup:
- Arduino (clone) with Ethernet shield to receive the commands
- led strip controller to control the rgb strip

I want to use the led controller to control the rgb strip because in this case I don't have to deal with PWM programming (to output the right color). This means the Arduino has to send the "remote ir" codes to the led controller.

I connect the Arduino and the strip controller with a opto-coupler (MB111 - from GDR) - [circuit](https://bitbucket.org/thomo/led-strip-remote/raw/master/images/circuit_stripdriver.jpg).

Web interface
-------------

The web interface is a small web page with some embedded javascript code. Javascript is needed to send GET and POST requests to the Arduino without reloading the web page.

There are three web pages:

* Off page - displayed when the strip is off
* On page - displayed when the strip is switched on 
* Config page - used to configure the controller

![webpages](https://bitbucket.org/thomo/led-strip-remote/raw/master/images/web_pages.jpg)

###Configuration

* Motion Detection (on/off) - to enable/disable the PIR sensors
* Motion Detection Color - which color or color mode should be used when strip is activated by PIR
* Automatic Timeout (in sec) - when a motion is detected, how long should the strip keep switched on
* Manual Timeout (in hour) - when switched on via the web site, how long should the strip keep on
* Standby Timeout (in min) - when switch off the strip the power supply of the strip will keep switched on for the given duration

###Finate State Machine

The programm is designed as a finate state machine. 

![fsm](https://bitbucket.org/thomo/led-strip-remote/raw/master/images/fsm.jpg)

The state transitions are triggered by

* PIR events - source PIR modules
* Web events - web interface
* Timer events - two software timer 
	* timeout timer - to switch off the strip and power supply after a timeout periode
	* strip on timer - to send a "strip on" ir command to strip every 250ms for 3 seconds after power supply is switched on (needed because it takes some time to bring up the power)


Arduino Pro Mini Clone
======================
Circuit
-------
![Circuit>](https://bitbucket.org/thomo/led-strip-remote/raw/master/images/circuit.jpg)

used additional hardware modules

* ENC28J60 Ethernet Module
* Relais Module - to switch on/off LED strip power supply
* 2x PIR modules

Connection Arduino Pro Mini to xxxENC28J60

* Pin 5V - VCC ENC28J60
* Pin GND - GND ENC28J60
* Pin 13 - SCK ENC28J60
* Pin 12 - MISO ENC28J60
* Pin 11 - MOSI ENC28J60
* Pin 8 - CS ENC28J60

Connection Arduino Pro Mini to other parts

* Pin 3 - IR Strip Output
* Pin 4 - Error LED
* Pin 5 - Relay Output
* Pin 7 - PIR Input

Used 3rd party libraries
------------------------
* [IRRemote](https://github.com/shirriff/Arduino-IRremote) - downloaded 2013-07-06 
* [EtherCard](https://github.com/jcw/ethercard) - downloaded 2013-09-08 
* [FinalStateMachine](http://playground.arduino.cc/Code/FiniteStateMachine) - downloaded 2013-09-15 from [here](http://arduino-info.wikispaces.com/HAL-LibrariesUpdates)
* [Timer](http://playground.arduino.cc/Code/Timer) - downloaded 2013-09-15 from [github](https://github.com/JChristensen/Timer)

The corresponding code is in the subfolder wattuino_remote. 

### Hardware setup

![Prototype](https://bitbucket.org/thomo/led-strip-remote/raw/master/images/prototype.jpg)
![final device setup](https://bitbucket.org/thomo/led-strip-remote/raw/master/images/hardwaresetup.jpg)

Proof of Concept - Arduino Uno with Ethernet Shield
===================================================
Controlling a LED strip controller (LED-SF-00297, bought 2012, Aldi Nord, Germany) using an Arduino Uno with Ethernet shield.

![Arduino and ethernetshield](https://bitbucket.org/thomo/led-strip-remote/raw/master/images/arduino_ethernetshield_driver_led_strip_controller.jpg)

The corresponding code is in the subfolder arduino_remote. 

Repository Content
==================

+ arduino_remote - remote control using an Arduino Uno with Ethernet shield
+ wattuino_remote - remote control using an Wattuino with ENC28J60 module
+ ir_decode - small arduino project to read out ir codes from ir controller 
+ libs - used 3rd party libraries