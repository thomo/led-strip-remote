function post(url, param, reload){
    var http = new XMLHttpRequest();
	
    http.open('POST', url + '?' + param, true);
	http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	http.onreadystatechange = reload ? function() {
		if(http.readyState == 4) {
			location.reload(true);
		}
	} : null;
	http.send(param);
}

function setState(state) {
	post('/state',state, true);
}

function sendIr(code) {
	post('/ir',code, false);
}

function postConfigValue(params){
    var http = new XMLHttpRequest();
	
    http.open('POST', '/config?'+params, true);
	http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	http.onreadystatechange = function() { 
		if(http.readyState == 4 && http.status == 200) {
			document.getElementById('message').innerHTML = http.responseText;
		}
	};
    http.send(params);
}

function setPirMode(mode) {
	postConfigValue('pir='+mode);
}

function setDefaultColor(color) {
	postConfigValue('color='+color);
}

function setAutoTmt(timeout) {
	postConfigValue('ato='+timeout);
}

function setManualTmt(timeout) {
	postConfigValue('mto='+timeout);
}

function setStandbyTmt(timeout) {
	postConfigValue('sto='+timeout);
}

function updateData(key, id) {
	var http = new XMLHttpRequest();
	
    http.open('GET', '/value/'+key, true);
	http.onreadystatechange = function() { 
		if(http.readyState == 4 && http.status == 200) {
			document.getElementById(id).innerHTML = http.responseText;
		}
	};
    http.send(null);
}
 
function update() {
	updateData("pir", "pir");
	updateData("color", "color");
	updateData("ato", "ato");
	updateData("mto", "mto");
	updateData("sto", "sto");
}