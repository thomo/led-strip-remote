def escape(line) 
  line.strip().gsub(/\"/,'\"')
end

begin
    if ARGV.size == 0
      raise "missing parameter"
    end
  
    input_file_name = ARGV[0]
    
    var_name = input_file_name.downcase.gsub(/\./,"_").gsub(/-/,"_");

    puts "#ifndef #{var_name}_h"
    puts "#define #{var_name}_h"
    puts ""
    
    puts "PROGMEM prog_char response_#{var_name}_content[] = "

    file = File.new(input_file_name, "r")

    pagelength = 0
    while (line = file.gets)
        pagelength += line.length
        eline = escape(line)
        next if eline.empty? 
        puts "\"#{eline}\\n\""
    end
    file.close
    puts "\"\";"
    
    puts "// Page length: #{pagelength}"
    
    puts "#endif"
rescue => err
    puts "Error while processing: #{err}"
    err
end