#!/bin/sh

ruby gen_progmem.rb index-on.html > res_index_on.h
ruby gen_progmem.rb index-off.html > res_index_off.h
ruby gen_progmem.rb config.html > res_config.h

ruby gen_progmem.rb my.css > res_css.h
ruby gen_progmem.rb my.js > res_js.h
