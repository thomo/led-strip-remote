require 'sinatra'
require 'pp'

state = 0;

get '/config' do
  File.read(File.join('config.html'))
end

get '/my.css' do
  content_type 'text/css', :charset => 'utf-8'
  File.read(File.join('my.css'))
end

get '/my.js' do
  content_type 'text/javascript', :charset => 'utf-8'
  File.read(File.join('my.js'))
end


get '/' do
  if state == 1
    File.read(File.join('index-on.html'))
  else
    File.read(File.join('index-off.html'))
  end
end

get '/value/:value' do
  content_type 'text/plain', :charset => 'utf-8'
  params[:value]+params[:value]
end

post '/state' do
  pp "#{params}"
  if params[:state] == "off" 
    state = 0
  else 
    state = 1
  end
  
  redirect "/", 303
end

post '/config' do
  pp "#{params}"
  "Set new value #{params}"
end

post '/ir' do
  pp "#{params}"
end
