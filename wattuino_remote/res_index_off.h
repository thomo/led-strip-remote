#ifndef index_off_html_h
#define index_off_html_h

PROGMEM prog_char response_index_off_html_content[] = 
"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n"
"<html  xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">\n"
"<head>\n"
"<meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\" />\n"
"<meta name=\"viewport\" content=\"width = 320, initial-scale = 0.8, user-scalable = no\">\n"
"<title>Led Strip Remote</title>\n"
"<link rel='stylesheet' href='/my.css' type='text/css' />\n"
"<script src=\"/my.js\" type=\"text/javascript\"></script>\n"
"</head>\n"
"<body>\n"
"<div style=\"margin-left:auto;margin-right:auto;width:360px;\">\n"
"<a href=\"#\" class=\"bt big bt-grey\" onClick=\"setState('on')\">ON</a>\n"
"</div>\n"
"</body>\n"
"</html>\n"
"";
// Page length: 661
#endif
