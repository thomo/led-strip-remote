/*
 * Based on sources from http://pragprog.com/titles/msard/source_code
 *   Arduino_1_0/RemoteControl/InfraredProxy
 */

#include <EtherCard.h>
#include <IRremote.h>
#include <FiniteStateMachine.h>
#include <Timer.h>

#include "web_event.h"

#include "res_index_on.h"
#include "res_index_off.h"
#include "res_config.h"
#include "res_css.h"
#include "res_js.h"

//#define DEBUG

#ifdef DEBUG
#define DEBUG_PRINT(str) Serial.println(str);
#define MINUTES 1 
#define HOURS 1
#else
#define DEBUG_PRINT(str)
#define MINUTES 60
#define HOURS 60
#endif

#define ETHERNET_BUFFER_LENGTH 200
byte Ethernet::buffer[ETHERNET_BUFFER_LENGTH];
char* netdata;

const int ERROR_PIN = 4;
const int RELAY_PIN = 5;
const int PIR_PIN = 7;

#define MAX_PATH_LENGTH 160
char path[MAX_PATH_LENGTH+1];

PROGMEM prog_char response_404[] = "HTTP/1.1 404 Not Found\nConnection: close\nContent-Type: text/html\n\r\n<h1>ERROR!</h1><br>Ressource not found: ";

PROGMEM prog_char response_data[] = "HTTP/1.1 200 OK\nConnection: close\n";

PROGMEM prog_char response_post_mode_with_redirect[] = "HTTP/1.1 303 OK\nLocation: /\n";

PROGMEM prog_char response_post_config[] = "HTTP/1.1 200 OK\nConnection: close\nContent-Type: text/html;charset=utf-8\nContent-Length: 7\n\r\nStored!\r\n";

PROGMEM prog_char response_plain[] = "HTTP/1.1 200 OK\nContent-Type: text/plain\n\r\n";

PROGMEM prog_char response_favicon_header[] = "HTTP/1.1 200 OK\nConnection: close\nContent-Length: 318\nContent-Type: image/x-icon\n\r\n";

PROGMEM prog_char favicon_red[] = {0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x10, 0x10, 0x10, 0x00, 0x01, 0x00, 0x04, 0x00, 0x28, 0x01, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x01, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x27, 0xd9, 0x00, 0x00, 0x0e, 0xb6, 0x00, 0x51, 0x9b, 0xfe, 0x00, 0x00, 0x36, 0xe7, 0x00, 0x00, 0x36, 0xf9, 0x00, 0xfc, 0xfd, 0xfc, 0x00, 0x00, 0x49, 0xfb, 0x00, 0x06, 0x09, 0x4e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x88, 0x88, 0x80, 0x00, 0x00, 0x00, 0x03, 0x82, 0x22, 0x22, 0x28, 0x30, 0x00, 0x00, 0x82, 0x14, 0x44, 0x44, 0x41, 0x88, 0x00, 0x03, 0x21, 0x44, 0x45, 0x54, 0x44, 0x18, 0x30, 0x02, 0x21, 0x45, 0x55, 0x55, 0x54, 0x12, 0x80, 0x22, 0x11, 0x55, 0x57, 0x75, 0x55, 0x41, 0x28, 0x22, 0x14, 0x55, 0x77, 0x77, 0x55, 0x41, 0x28, 0x22, 0x15, 0x57, 0x33, 0x33, 0x75, 0x51, 0x28, 0x22, 0x15, 0x73, 0x36, 0x63, 0x37, 0x51, 0x28, 0x22, 0x14, 0x73, 0x36, 0x63, 0x37, 0x41, 0x28, 0x32, 0x11, 0x73, 0x36, 0x63, 0x37, 0x11, 0x28, 0x02, 0x21, 0x47, 0x33, 0x33, 0x74, 0x12, 0x80, 0x03, 0x21, 0x44, 0x73, 0x37, 0x44, 0x18, 0x30, 0x00, 0x22, 0x14, 0x33, 0x33, 0x71, 0x88, 0x00, 0x00, 0x03, 0x22, 0x21, 0x12, 0x28, 0x30, 0x00, 0x00, 0x00, 0x03, 0x88, 0x88, 0x80, 0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00, 0xe0, 0x07, 0x00, 0x00, 0xc0, 0x03, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0xc0, 0x03, 0x00, 0x00, 0xe0, 0x07, 0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00};
PROGMEM prog_char favicon_grn[] = {0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x10, 0x10, 0x10, 0x00, 0x01, 0x00, 0x04, 0x00, 0x28, 0x01, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00, 0x28, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x01, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0xf8, 0xd1, 0x00, 0x00, 0xb0, 0x5a, 0x00, 0x00, 0xc9, 0x73, 0x00, 0x00, 0xd6, 0x86, 0x00, 0x00, 0xed, 0x8e, 0x00, 0xfc, 0xfd, 0xfc, 0x00, 0x0a, 0x51, 0x2b, 0x00, 0x03, 0xb4, 0x67, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x77, 0x77, 0x70, 0x00, 0x00, 0x00, 0x08, 0x77, 0x28, 0x82, 0x77, 0x80, 0x00, 0x00, 0x77, 0x84, 0x44, 0x44, 0x48, 0x77, 0x00, 0x04, 0x23, 0x44, 0x45, 0x54, 0x44, 0x37, 0x80, 0x07, 0x84, 0x45, 0x55, 0x55, 0x54, 0x48, 0x70, 0x82, 0x34, 0x55, 0x55, 0x55, 0x55, 0x43, 0x77, 0x22, 0x34, 0x55, 0x11, 0x11, 0x55, 0x43, 0x27, 0x22, 0x34, 0x51, 0x11, 0x11, 0x15, 0x43, 0x27, 0x22, 0x34, 0x51, 0x16, 0x61, 0x15, 0x43, 0x27, 0x82, 0x34, 0x51, 0x66, 0x66, 0x15, 0x43, 0x27, 0x88, 0x83, 0x51, 0x16, 0x61, 0x15, 0x33, 0x77, 0x02, 0x23, 0x45, 0x11, 0x11, 0x54, 0x32, 0x70, 0x08, 0x28, 0x44, 0x51, 0x15, 0x44, 0x87, 0x80, 0x00, 0x82, 0x84, 0x45, 0x54, 0x48, 0x77, 0x00, 0x00, 0x08, 0x22, 0x88, 0x88, 0x77, 0x80, 0x00, 0x00, 0x00, 0x08, 0x77, 0x77, 0x80, 0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00, 0xe0, 0x07, 0x00, 0x00, 0xc0, 0x03, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0x80, 0x01, 0x00, 0x00, 0xc0, 0x03, 0x00, 0x00, 0xe0, 0x07, 0x00, 0x00, 0xf8, 0x1f, 0x00, 0x00};

static byte mymac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
static byte myip[] = { 10, 0, 0, 252 };

// runtime configuration 
char auto_color[3] = "61";

boolean pir_enabled = true;

unsigned long tmr_auto_sec = 15; 
unsigned long tmr_standby_min = 5;
unsigned long tmr_manual_hour = 4;

// --------------------------------------------------------------------------------------------------

State StateOff = State(enterOff, updateOff, exitOff);
State StateStandby = State(enterStandby, updateStandby, exitStandby);
State StateAuto = State(enterAuto, updateAuto, exitAuto);
State StateManual = State(enterManual, updateManual, NULL);
 
FSM fsm = FSM(StateOff);     


// --------------------------------------------------------------------------------------------------

class InfraredProxy {
  IRsend _infrared_sender;
  
  void sendNEC32(const char* ir_code) {
    long code = strtol(ir_code,NULL,16);
    long cmd = 0x00FF0000 | (code << 8) | (0x000000FF & ~code);
    _infrared_sender.sendNEC(cmd, 32);
    delay(20);
  }
  
  public:
  
  void showColor(const char* code) {
    sendNEC32(code);
  }
  
  void stripOff() {
    DEBUG_PRINT("Strip OFF");
    sendNEC32("41");
  }

  void stripOn() {
    DEBUG_PRINT("Strip ON");
    sendNEC32("c1");
  }

  void showAutoColor() {
    sendNEC32(auto_color);
  }
};

InfraredProxy ir_proxy;

// --------------------------------------------------------------------------------------------------

Timer tmr;
int tmrEvent = NO_TIMER_AVAILABLE;
int tmrStripOn = NO_TIMER_AVAILABLE;

void tmrEvent_Triggered() {
  if (fsm.isInState(StateStandby)) {
    fsm.transitionTo(StateOff);
  } else {
    fsm.transitionTo(StateStandby);
  }
}

void timerOn(unsigned long period) {
  tmr.stop(tmrEvent);
  tmrEvent = tmr.after(period, tmrEvent_Triggered);
}

void timerOnAuto() {
  timerOn(tmr_auto_sec * 1000);
}

void timerOnStandby() {
  timerOn(tmr_standby_min * 1000 * MINUTES);
}

void timerOnManual() {
  timerOn(tmr_manual_hour * 1000 * MINUTES * HOURS);
}

void tmrStripOn_Triggered() {
  DEBUG_PRINT(".");
  ir_proxy.stripOn();
}

void timerStripOn() {
  tmr.stop(tmrStripOn);
  tmrStripOn = tmr.every(250, tmrStripOn_Triggered, 12);
}


void timerOff() {
  DEBUG_PRINT("tmr stop");
  tmr.stop(tmrEvent);
  tmrEvent = NO_TIMER_AVAILABLE;
  tmr.stop(tmrStripOn);
  tmrStripOn = NO_TIMER_AVAILABLE;
}

// --------------------------------------------------------------------------------------------------

/**
 * Checks if ethercard has received data.
 *
 **/
boolean hasWebRequest() {
  word len = ether.packetReceive();
  word pos = ether.packetLoop(len);
  
  if (pos) { // check if valid tcp data is received
    netdata = (char *) Ethernet::buffer + pos;
    extractPath(&netdata[5]);
    DEBUG_PRINT(path);
    return true;
  }
  return false;
}

/**
 * Check if data are a GET request.
 * In case of a GET request the get path is copied into globale variable path
 */
boolean hasGetRequest() {
  if (strncmp("GET /", netdata, 5) == 0) {
      extractPath(&netdata[4]);
      return true;
  } 
  return false;
}

boolean hasPostRequest() {
  if (strncmp("POST /", netdata, 6) == 0) {
      extractPath(&netdata[5]);
      return true;
  } 
  return false;
}

/**
 * data - REQUEST Header
 * path - extracted path from first line (GET/POST line), 
 *    e.g. "GET /favicon.ico HTTP/1.1" results in "/favicon.ico"
 *    
 **/
void extractPath(const char* source) {
  int i = 0;
  while(source[i] != ' ' && i < MAX_PATH_LENGTH) {
    path[i] = source[i];
    ++i;
  }
  path[i] = 0;  
}

// --------------------------------------------------------------------------------------------

void sendHeaderReply_P(const char *header_content, int header_content_len, int flags) {
  ether.httpServerReplyAck();

  memcpy_P(ether.tcpOffset(), header_content, header_content_len); 
  ether.httpServerReply_with_flags(header_content_len - 1, flags);
}

void handleDataRequest() {
  sendHeaderReply_P(response_data, sizeof response_data, TCP_FLAGS_ACK_V|TCP_FLAGS_FIN_V);
}

// --------------------------------------------------------------------------------------------

void sendContent_P(const char *content, int contentsize) {
  int idx = 0;
  int packetlen;
  while(idx < contentsize) {
    packetlen = min(ETHERNET_BUFFER_LENGTH-100, (contentsize - idx));
    memcpy_P(ether.tcpOffset(), &content[idx], packetlen); 
    idx += packetlen;
    
    if (idx != contentsize) {
      ether.httpServerReply_with_flags(packetlen, TCP_FLAGS_ACK_V);
    } else {
      ether.httpServerReply_with_flags(packetlen, TCP_FLAGS_ACK_V|TCP_FLAGS_FIN_V);
    }
  }
}

void sendWebpage_P(const char *content, int pagelen, char* contentType) {
  ether.httpServerReplyAck();

  char header[100];
  sprintf(header, "HTTP/1.1 200 OK\nContent-Type: %s\nContent-Length: %d\n\r\n", contentType, pagelen);
  memcpy(ether.tcpOffset(), header, strlen(header)); 
  ether.httpServerReply_with_flags(strlen(header), TCP_FLAGS_ACK_V);
  
  sendContent_P(content, pagelen);
}

void sendFavicon() {
  sendHeaderReply_P(response_favicon_header, sizeof response_favicon_header, TCP_FLAGS_ACK_V);
  sendContent_P(fsm.isInState(StateOff) || fsm.isInState(StateStandby) ? favicon_red : favicon_grn, 318);
}

void send404(const char* path) {
  sendHeaderReply_P(response_404, sizeof response_404, TCP_FLAGS_ACK_V);

  memcpy(ether.tcpOffset(), path, strlen(path));
  ether.httpServerReply_with_flags(strlen(path), TCP_FLAGS_ACK_V|TCP_FLAGS_FIN_V);
}

void handleHomePageRequest() {
  if (fsm.isInState(StateOff) || fsm.isInState(StateStandby)) {
    sendWebpage_P(response_index_off_html_content, sizeof response_index_off_html_content - 1, "text/html");
  } else {
    sendWebpage_P(response_index_on_html_content, sizeof response_index_on_html_content - 1, "text/html");
  }
}

void sendPlainValue(const char* key) {
  sendHeaderReply_P(response_plain, sizeof response_plain, TCP_FLAGS_ACK_V);
  
  char value[5];

  if (strcmp(key, "pir") == 0) {
    // motion detetion mode
    sprintf(value, pir_enabled ? "on" : "off");
  } else if (strcmp(key, "color") == 0) {
    // motion detetion color
    sprintf(value, "%s", auto_color);
  } else if (strcmp(key, "ato") == 0) {
    // automatic timeout
    sprintf(value, "%u", tmr_auto_sec);
  } else if (strcmp(key, "mto") == 0) {
    // manual timeout
    sprintf(value, "%u", tmr_manual_hour);
  } else if (strcmp(key, "sto") == 0) {
    // standby timeout
    sprintf(value, "%u", tmr_standby_min);
  } else {
    sprintf(value, "???");
  }
  
  memcpy(ether.tcpOffset(), value, strlen(value)); 
  ether.httpServerReply_with_flags(strlen(value), TCP_FLAGS_ACK_V|TCP_FLAGS_FIN_V);
}

// --------------------------------------------------------------------------------------------

void postConfigResponse() {
  sendHeaderReply_P(response_post_config, sizeof response_post_config, TCP_FLAGS_ACK_V|TCP_FLAGS_FIN_V);
}

void handleConfigPost(const char* params) {
  if (strncmp("ato=", params, 4) == 0) {
    tmr_auto_sec = strtol(&params[4],NULL,10);
    postConfigResponse();
  } else if (strncmp("mto=", params, 4) == 0) {
    tmr_manual_hour = strtol(&params[4],NULL,10);
    postConfigResponse();
  } else if (strncmp("sto=", params, 4) == 0) {
    tmr_standby_min = strtol(&params[4],NULL,10);
    postConfigResponse();
  } else if (strncmp("color=", params, 6) == 0) {
    strncpy(auto_color, &params[6], 2);
    postConfigResponse();
  } else if (strcmp("pir=on", params) == 0) {
    pir_enabled = true;
    postConfigResponse();
  } else if (strcmp("pir=off", params) == 0) {
    pir_enabled = false;
    postConfigResponse();
  } else {
    handleDataRequest();
  }
 }

WebEvent handleStatePost(const char* params) {
  if (strcmp("on", params) == 0) {
    sendHeaderReply_P(response_post_mode_with_redirect, sizeof response_post_mode_with_redirect, TCP_FLAGS_ACK_V|TCP_FLAGS_FIN_V);
    return POST_STATE_ON;
  } else if (strcmp("off", params) == 0) {
    sendHeaderReply_P(response_post_mode_with_redirect, sizeof response_post_mode_with_redirect, TCP_FLAGS_ACK_V|TCP_FLAGS_FIN_V);
    return POST_STATE_OFF;
  } else {
    handleDataRequest();
    return INVALID;
  }
}

void handleIrCodePost(const char* params) {
  ir_proxy.showColor(params);
  handleDataRequest();
}

WebEvent handlePostRequest() {
  WebEvent event = INVALID;
  if (strncmp("/state?", path, 7) == 0) {
    event = handleStatePost(&path[7]);
  } else if (strncmp("/ir?", path, 4) == 0) {
    handleIrCodePost(&path[4]);
    event = POST_IR;
  } else if (strncmp("/config?", path, 8) == 0) {
    handleConfigPost(&path[8]);
    event = POST_CONFIG;
  } else {
    send404(path);
  }
  return event;
}
// --------------------------------------------------------------------------------------------

WebEvent processWebEvent() {
  WebEvent event = INVALID;
  if (hasWebRequest()) {
    if (hasGetRequest()) {
      event = GET;
      if (strcmp("/", path) == 0) {
        handleHomePageRequest();
      } else if (strcmp("/my.css", path) == 0) {
        sendWebpage_P(response_my_css_content, sizeof response_my_css_content - 1, "text/css");
      } else if (strcmp("/my.js", path) == 0) {
        sendWebpage_P(response_my_js_content, sizeof response_my_js_content - 1, "text/javascript;charset=utf-8");
      } else if (strcmp("/config", path) == 0) {
        sendWebpage_P(response_config_html_content, sizeof response_config_html_content - 1, "text/html");
      } else if (strncmp("/value/", path,7) == 0) {
        sendPlainValue(&path[7]);
      } else if (strcmp("/favicon.ico", path) == 0) {
        sendFavicon();
      } else {
        send404(path);
      }
    } 
    else if (hasPostRequest()) {
      event = handlePostRequest();
    }
  }
  return event;
}

// --------------------------------------------------------------------------------------------------

boolean isPirEvent() {
  return (pir_enabled && digitalRead(PIR_PIN));
}

// --------------------------------------------------------------------------------------------------

void powerOff() {
  digitalWrite(RELAY_PIN, HIGH);
}

void powerOn() {
  digitalWrite(RELAY_PIN, LOW);
}

// --------------------------------------------------------------------------------------------------

void enterOff() {
  DEBUG_PRINT("> off");
  powerOff();
}

void updateOff() {
  if (isPirEvent()) {
    fsm.transitionTo(StateAuto);
  }
  
  WebEvent event = processWebEvent();
  if (event == POST_IR || event == POST_STATE_ON) {
    fsm.transitionTo(StateManual);
  }
}

void exitOff() {
  DEBUG_PRINT("< off");
  powerOn();
  timerStripOn();
}

// +++++++++++++++++++++++++++++++++++

void enterStandby() {
  DEBUG_PRINT("> standby");
  ir_proxy.stripOff();
  timerOnStandby();
}

void updateStandby() {
  if (isPirEvent()) {
    ir_proxy.stripOn();
    fsm.transitionTo(StateAuto);
  }
  
  WebEvent event = processWebEvent();
  if (event == POST_IR || event == POST_STATE_ON) {
    ir_proxy.stripOn();
    fsm.transitionTo(StateManual);
  }
}

void exitStandby() {
  DEBUG_PRINT("< standby");
  timerOff();
}

// +++++++++++++++++++++++++++++++++++

void enterAuto() {
  DEBUG_PRINT("> auto");
  timerOnAuto();
  ir_proxy.showAutoColor();
}

void updateAuto() {
  if (isPirEvent()) {
    timerOnAuto();
    ir_proxy.showAutoColor();
  }
  
  WebEvent event = processWebEvent();
  if (event == POST_IR || event == POST_STATE_ON) {
    fsm.transitionTo(StateManual);
  } else if (event == POST_STATE_OFF) {
    fsm.transitionTo(StateStandby);
  }
}

void exitAuto() {
  DEBUG_PRINT("< auto");
  timerOff();
}

// +++++++++++++++++++++++++++++++++++

void enterManual() {
  DEBUG_PRINT("> manual");
  timerOnManual();
}

void updateManual() {
  WebEvent event = processWebEvent();
  if (event == POST_IR || event == POST_STATE_ON) {
    timerOnManual();
  } else if (event == POST_STATE_OFF) {
    fsm.transitionTo(StateStandby);
  }
}

void exitManual() {
  DEBUG_PRINT("< manual");
  timerOff();
}

// --------------------------------------------------------------------------------------------------
void setup() {
  pinMode(PIR_PIN, INPUT);
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(ERROR_PIN, OUTPUT);

  powerOff();

#ifdef DEBUG
  Serial.begin(57600);
  DEBUG_PRINT("(RE)START");
#endif
  
  if (ether.begin(sizeof Ethernet::buffer, mymac) == 0) {
    digitalWrite(ERROR_PIN, HIGH);
  } else {
    digitalWrite(ERROR_PIN, LOW);
  }  
  ether.staticSetup(myip);
}

void loop() {
  tmr.update();
  fsm.update();
}
 
